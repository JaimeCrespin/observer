import Foundation

protocol Observer {
    var observerID: Int{ get }
    func update( key : String)
}

private var observers = [Observer]()
func register(observer: Observer){
    observers.append(observer)
}
func unregister(observer: Observer){
    observers = observers.filter{
        $0.observerID != observer.observerID
    }
}
func notifyObserver(key: String){
    for observer in observers {
        observer.update(key: key)
    }
}

class mrJames: Observer {
    var observerID: Int = 1
    init() {
        register(observer: self)
    }
    func update(key: String){
        print("James ha recibido la notificacion para jugar, pero la ignora.")
    }
    deinit {
        unregister(observer: self)
    }
}
class mrMakarov: Observer {
    var observerID: Int = 2
    init() {
        register(observer: self)
    }
    func update(key: String) {
        print("Makarov ha recibido la notificacion y la acepta.")
        switch key {
        case "llamadoAmanecidota":
            doCall()
            break
        default:
            break;
        }
    }
    func doCall(){
        print("Los demas amigos empiezan el spam de invitaciones hacia James.")
    }
    deinit {
        unregister(observer: self)
    }
}

let boss = mrJames()
let dho = mrMakarov()
notifyObserver(key:"llamadoAmanecidota")
